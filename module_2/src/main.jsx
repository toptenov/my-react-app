import './index.css';
import { TermList } from "./TermList";
import { createRoot } from "react-dom/client";

let terms = restoreTermList();

const descriptionList = document.getElementById('description-list');
const reactRoot = createRoot(descriptionList);
const form = document.getElementById('add-description');

const saveTermList = (terms) => localStorage.setItem("termList", JSON.stringify(terms));

function restoreTermList() {
  const rawTermList = localStorage.getItem("termList");
  return rawTermList ? JSON.parse(rawTermList) : [];
}

function syncTermList() {
  saveTermList(terms);
  reactRoot.render(<TermList terms={terms} onDelete={deleteItem} />);
};

function addTerm(title, description) {
  terms.push(
    {
      id: crypto.randomUUID(),
      title,
      description
    }
  );

  terms.sort((term1, term2) => (term1.title < term2.title ? -1 : 1));
  syncTermList();
};

function deleteItem(id) {
  terms = terms.filter(term => term.id !== id);
  syncTermList();
};

syncTermList();

form.addEventListener('submit', (event) => {
  event.preventDefault();

  const title = form.elements['title'].value;
  const description = form.elements['description'].value;

  form.reset();

  addTerm(title, description);
});

