import './App.css';
import { Counter } from './Counter';
import { FocusableInput } from './FocusableInput';
import { Form } from './Form';
import { SearchableList } from './SearchableList';
import { Stopwatch } from './Stopwatch';

const list = [
  {
    id: '1',
    title: 'Арбуз'
  },
  {
    id: '2',
    title: 'Огурец'
  },
  {
    id: '3',
    title: 'Свёкла'
  }
]

function App() {
  return (
    <>
      <SearchableList list={list} /><br /><hr /><br />
      <Counter /><br /><hr /><br />
      <Form /><br /><hr /><br />
      <FocusableInput /><br /><hr /><br />
      <Stopwatch />
    </>
  );
};

export default App
