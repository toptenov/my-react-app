import { useState } from "react";

export const Counter = () => {
    const [count, setCount] = useState(0);

    const increment = () => {
        setCount(count + 1);
    };

    const doubleIncrement = () => {
        setCount(count + 2);
    };

    return (
        <div>
            <span>Значение счётчика: {count}</span>
            <button onClick={increment}>Увеличить на 1</button>
            <button onClick={doubleIncrement}>Увеличить на 2</button>
        </div>
    );
};