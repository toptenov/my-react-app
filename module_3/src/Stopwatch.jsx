import { useEffect, useState } from "react";

export const Stopwatch = () => {
    const seconds = useStopwatch();

    return (
        <div>Прошло {seconds} секунд</div>
    );
};

function useStopwatch() {
    const [seconds, setSeconds] = useState(0);

    useEffect(() => {
        const timerId = setInterval(() => {
            console.count("Stopwatch");
            setSeconds((seconds) => seconds + 1);
        }, 1000);

        return () => {clearInterval(timerId)};
    }, []);

    return seconds;
};