import { useState } from "react";

export const Form = () => {
    const [user, setUser] = useState(
        {
            firstName: 'Иван',
            lastName: 'Иванов'
        }
    );

    const handleFirstNameChange = (event) => {
        setUser(
            {
                ...user,
                firstName: event.target.value
            }
        );
    };

    return (
        <form >
            <span>{user.firstName} {user.lastName}</span><br />
            <input
                type="text"
                onChange={handleFirstNameChange}
            />
        </form>
    );
};